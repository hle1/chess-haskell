import ChessGame 
import TestGames
import Data.Maybe
import Data.Char
import System.IO
import System.Environment
import System.Console.GetOpt

data Flag = Help | Winner | CutOff String | Move String  | Verbose | Interactive deriving (Eq,Show)

options:: [OptDescr Flag]
options = 
 [ Option ['h'] ["help"] (NoArg Help) "Print out a help message and quit the program.",
   Option ['w'] ["winner"] (NoArg Winner) "Print out who will win this game, using an exhaustive search.",
   Option ['d'] ["depth"] (ReqArg CutOff "num") "Use num as a cutoff depth.",
   Option ['m'] ["move"] (ReqArg Move "pos") "Template move: 'Piece,CurrPos,NewPos'. With [Pawn, Knight, Bishop, Rook, Queen, King] = [P,N,B,R,Q,K], [a..h] = x-coordination, [1..8] = y-coordination. Ex: Pe2e4",
   Option ['v'] ["verbose"] (NoArg Verbose) "Output move and show how good it is",
   Option ['i'] ["interactive"] (NoArg Interactive) "Start a new game and play against computer"
 ]

main = do
--   putStrLn "Welcome to Chess Game"
--   putStrLn "---------------------"
   args <- getArgs
   let (flags, others, errors) = getOpt Permute options args
   if (Help `elem` flags)
   then putStrLn $ usageInfo "Chess Game Instruction:" options
   else if (Interactive `elem`flags) 
        then do showGame newGame 
                againstAI flags
        else do game <- getGame others
                case gameResult game of
                    Just result -> print $ result
                    Nothing -> loopGame game flags


getGame:: [String] -> IO Game
getGame [] =  askGame
getGame (x:xs) = loadGame x

askGame:: IO Game
askGame = do
    putStrLn "Please enter a game file: "
    str <- getLine
    loadGame str
    

loopGame game flags = do 
  if (Winner `elem` flags)
  then do print $ whoWillWin game
  else if (Verbose `elem` flags) 
       then do str <- getMove flags
               if (str /= [])       
               then do let newGame = parseMove game str
                       case newGame of
                           Nothing -> do putStrLn "Not a valid move!"
                           Just g -> do showGame g
                                        putStr "Score of current game: "
                                        print $ whoMightWin 2 g 
               else do depth <- getDepth flags
                       let newGame = playAiMove game depth
                       showGame newGame
                       putStr "Score of current game: "
                       print $ whoMightWin depth newGame

       else do str <- getMove flags
               if (str /= [])  
               then do let newGame = parseMove game str
                       case newGame of 
                             Nothing -> do putStrLn "Not a valid move!" 
                             Just g -> print $ g
               else do depth <- getDepth flags
                       print $ playAiMove game depth

          
getMove:: [Flag] -> IO String 
getMove [] = return []
getMove ((Move str):_) = return str
getMove (_:xs) = getMove xs

   
getDepth:: [Flag] -> IO Int
getDepth [] = return 2 
getDepth ((CutOff d):_) = return $ read d
getDepth (_:xs) = getDepth xs

againstAI flags = do 
   depth <- getDepth flags
   putStrLn "What color do you want to play? White or Black?"
   color <- getLine 
   if (color `elem` ["White", "white", "w"]) then gameHuman newGame depth
   else if (color `elem` ["Black", "black", "b"]) then gameAI newGame depth
        else do putStrLn "Not valid, try again!"
                againstAI flags

gameHuman game depth = do
   putStrLn "Your move: "
   str <- getLine
   if (str == "quit") then putStrLn "Thanks for playing!"  
   else case parseMove game str of
        Nothing ->
                do putStrLn "Not a valid move, try again!!"
                   gameHuman game depth
        Just newGame ->
                   do showGame newGame
                      case gameResult newGame of
                        Nothing -> gameAI newGame depth
                        Just (Won pcolor) -> putStrLn  "You won the game!!!"
                        Just Tie -> putStrLn "It's a tie!!!"

gameAI game depth = do 
   putStrLn "Machine is thinking........."
   let newGame = playAiMove game depth
   showGame newGame
   case gameResult newGame of
       Nothing -> gameHuman newGame depth
       Just (Won pcolor) -> putStrLn "You lost the game!!!"
       Just Tie -> putStrLn "It's a tie!!!"

parseMove:: Game -> String -> Maybe Game
parseMove game@(Game b m pcolor) str =
  case str of
     (p:x1:y1:x2:y2:[]) ->
                case toRank p of
                    Nothing -> Nothing
                    Just rank -> let  piece = (Piece pcolor rank (rankToInt x1, digitToInt y1))
                                      newPos = (rankToInt x2, digitToInt y2)
                                 in moveGame game (piece, newPos)
     other -> Nothing

loadGame :: FilePath -> IO Game
loadGame path = do
    str <- readFile path
    readIO str

saveGame :: FilePath -> Game -> IO ()
saveGame path gs = writeFile path $ show gs

readGame:: String -> Game
readGame str =
   let (x:x1:xs) = words str
   in (Game (readPieces xs) (read x::Int) (toPlayer x1))

readPieces :: [String] -> [Piece]
readPieces [] = []
readPieces (x1:x2:x3:x4:xs) =
   let recSol = readPieces xs
   in (Piece (toPlayer x1) (read x2::Rank) ((read x3::Int), (read x4::Int))):recSol
