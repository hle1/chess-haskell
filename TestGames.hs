module TestGames where 

game1 = unlines ["3", "White", "White King 7 3", "White Rook 1 3", "Black King 8 1"]

game2 = unlines ["5", "Black", "White King 2 1", "White Pawn 8 2", "Black King 2 3", "Black Bishop 5 2", "Black Bishop 4 2"]

game2a = unlines ["5", "Black", "White King 2 1", "Black King 2 3", "Black Bishop 5 2", "Black Bishop 4 2"]

game2b = unlines ["4", "White", "Black Bishop 4 3", "White King 2 1", "Black King 2 3", "Black Bishop 4 2"]

game3 = unlines ["3", "Black", "White King 3 5", "Black King 1 1", "Black Rook 1 5", "Black Queen 2 3"]

game4 = unlines ["9", "White", "White King 3 8", "Black King 1 6", "White Bishop 3 6", "Black Pawn 1 7", "Black Pawn 1 5", "Black Pawn 1 4"]

game5 = unlines ["5", "White", "White King 1 1", "Black King 6 8", "White Rook 1 7", "White Rook 1 6"]
