module ChessGame where

import Data.Maybe
import Data.List
import Data.Char
import Data.Tuple
import Debug.Trace


type Position = (Int, Int)
data Rank = Pawn | Knight | Bishop | Rook | Queen | King deriving (Show, Read, Eq, Ord)
data Piece = Piece Player Rank Position deriving (Show, Eq, Read, Ord)
data Player = Black | White deriving (Show, Eq, Read, Ord)
type Board = [Piece]
data Game = Game Board Int Player deriving (Show, Eq, Read)
data Result = Won Player | Tie deriving (Show,Eq, Read)
type Move = (Piece, Position) 

opponent :: Player -> Player
opponent White = Black
opponent Black = White

toPlayer:: String -> Player
toPlayer p = case p of
                   "White" -> White
                   "Black" -> Black
toRank:: Char -> Maybe Rank
toRank p = case p of
                       'P' -> Just Pawn
                       'N' -> Just Knight
                       'B' -> Just Bishop
                       'R' -> Just Rook
                       'Q' -> Just Queen
                       'K' -> Just King
                       other -> Nothing
rankToInt:: Char -> Int
rankToInt r = case r of
                   'a' -> 1
                   'b' -> 2
                   'c' -> 3
                   'd' -> 4
                   'e' -> 5
                   'f' -> 6
                   'g' -> 7
                   'h' -> 8
                   other -> 9


blankBoard:: Board
blankBoard = [] 

newGame:: Game
newGame = Game setupBoard 60 White
loc:: Position -> Board -> Maybe Piece 
loc (x,y) b  = find (\p -> locPiece p == (x,y)) b

showBoard:: Board -> [String]
showBoard b = 
  [[ case loc (x,y) b of 
         Just p  -> showPiece p
         Nothing -> '.'
     | x<-[1..8]]| y <- [1..8]]

showPrettyBoard board = do
   mapM_ putStrLn $ map (intersperse ' ') $ zipWith (:) ['8','7'..] $ zipWith (:) (replicate 8 '|') (reverse (showBoard( board)))

   putStrLn "   ________________"
   putStrLn $ intersperse ' ' $ ' ':' ':['a' ..'h']

showGame (Game b move player) = do showPrettyBoard b 

-- Initial Board State
setupBoard :: [Piece]
setupBoard =
  [Piece White Pawn (x, 2)|x<-[1..8]] ++
  [Piece Black Pawn (x,7)|x<-[1..8]] ++
    zipWith (Piece Black) (map (\l->read l) (words "Rook Knight Bishop Queen King Bishop Knight Rook")) [(i,8)|i<-[1..8]] ++
    zipWith (Piece White) (map (\l->read l) (words "Rook Knight Bishop Queen King Bishop Knight Rook")) [(i,1)|i<-[1..8]]

locPiece:: Piece -> Position
locPiece (Piece c t pos) = pos


-- Represents pieces as chars in the list of list
showPiece:: Piece -> Char
showPiece (Piece Black Pawn x) = '♙'
showPiece (Piece Black Knight x) = '♘'
showPiece (Piece Black Bishop x) = '♗'
showPiece (Piece Black Rook x) = '♖'
showPiece (Piece Black Queen x) = '♕'
showPiece (Piece Black King x) = '♔'

showPiece (Piece White Pawn x) = '♟'
showPiece (Piece White Knight x) = '♞'
showPiece (Piece White Bishop x) = '♝'
showPiece (Piece White Rook x) = '♜'
showPiece (Piece White Queen x) = '♛'
showPiece (Piece White King x) = '♚'


getColor:: Piece -> Player
getColor (Piece White t l) = White
getColor (Piece Black t l) = Black

dropPiece:: Board -> Piece -> Maybe Board 
dropPiece [] p = Nothing
dropPiece (x:xs) p = if x == p
                       then Just xs else fmap (x:) (dropPiece xs p)

legalMove:: Board -> Move -> Bool
legalMove board (piece@(Piece pcolor ptype (x1,y1)), pos@(x2,y2)) = 
  let dX = abs (x2-x1) --distance x
      dY = abs (y2-y1) --distance y
      between a b = case compare a b of 
                        LT -> [a..b]
                        GT -> reverse [b..a]
                        EQ -> [a]
      betweenPos = if (dX == dY) 
                   then [(x,y) | x<- between x1 x2, y<- between y1 y2, (y1-y)*dX==(x1-x)*dY||(y1-y)*dX==(-(x1-x)*dY)] 
                   else [(x,y) | x<- between x1 x2, y<- between y1 y2]        
      betweenLoc = map (\pos -> loc pos board) betweenPos
      betweenPieces = nub $ tail (init betweenLoc) 
  in
--move within the board
      x2 `elem` [1..8] && y2 `elem` [1..8] 
   && (dX/=0 || dY/=0) 
--Basic Movement for each piece
   && case ptype of
        Pawn -> (dX == 0 && (nub (tail betweenLoc) == [Nothing]) &&
                   if pcolor==White then (y2-y1 == 1) || (y1==2 && y2-y1 == 2)
                   else (y2-y1==(-1)) || (y1==7 && y2-y1==(-2)))
                || (dX==dY && dY==1) && (tail betweenLoc /= [Nothing]) 
                    && if pcolor==White then (y2-y1 == 1) 
                       else (y2-y1==(-1))
        Knight -> dX/=0 && dY /=0 && dX+dY==3
        Bishop -> dX == dY
        Rook -> dX==0 || dY==0
        Queen -> (dX==dY) || (dX==0 || dY==0)
        King -> ((dX+dY == 1) && (dX == 0 || dY == 0)) || ((dX == dY) && (dX == 1))
-- Can't go to the same color
   && case loc pos board of
        Nothing -> True
        Just p -> getColor p /= pcolor   
   && ((betweenPieces == [Nothing] || betweenPieces == []) || ptype ==Knight || ptype == Pawn)
     

move:: Board -> Move -> Maybe Board
move b (p@(Piece c t pos), newPos) = 
       if (legalMove b (p, newPos)) 
       then let newPiece = Piece c t newPos 
            in fmap (newPiece:) $ (dropPiece (filter (\piece -> locPiece piece /= newPos) b) p)
       else Nothing

gameResult:: Game -> Maybe Result
gameResult (Game b move player) = 
   let bpieces = filter (\p -> (getColor p) == Black) (b)
       wpieces = filter (\p -> (getColor p) == White) (b)
       valWhite = sum $ map (\p -> value p) wpieces
       valBlack = sum $ map (\p -> value p) bpieces
   in if (valWhite < 1000 && valBlack > 1000) 
      then Just (Won Black) 
      else if (valBlack < 1000 && valWhite >1000)
           then Just (Won White) 
           else if (move == 0)
                then Just Tie
                else Nothing

evaluate:: Game -> Maybe Player 
evaluate (Game b move player) = 
   let bpieces = filter (\p -> (getColor p) == Black) (b)
       wpieces = filter (\p -> (getColor p) == White) (b)
       valWhite = sum $ map (\p -> value p) bpieces
       valBlack = sum $ map (\p -> value p) wpieces
   in case compare valWhite valBlack of 
         GT -> Just Black  
         LT -> Just White
         EQ -> Nothing

evaluateGame:: Game -> Int
evaluateGame (Game b move player) = 
   let bpieces = filter (\p -> (getColor p) == Black) (b)
       wpieces = filter (\p -> (getColor p) == White) (b)
       valWhite = sum $ map (\p -> value p) wpieces
       valBlack = sum $ map (\p -> value p) bpieces
   in valWhite - valBlack

value:: Piece -> Int
value (Piece pcolor ptype loc) =
              case ptype of 
                    Pawn -> 1
                    Knight -> 3
                    Bishop -> 3
                    Rook -> 5
                    Queen -> 9 
                    King -> 1000

--Find all possible moves
findValidMoves:: Game -> [Move]
findValidMoves (Game board m player)= concat allMoves
  where allMoves = map (\p@(Piece pcolor ptype pos) -> if (pcolor==player) then (pieceMoves p) else [] ) board
        allPos = [(x,y) | x<- [1..8], y<-[1..8]]
        pieceMoves p =  catMaybes $ map (\pos -> if (legalMove board  (p, pos)) then Just (p, pos) else Nothing) allPos


--Find the best move. Just return th emove, or a Maybe Move.
findBestMove:: Game -> (Move, Result)
findBestMove game@(Game board int player) =  bestFor player (allResults (allGamesM game))
     where allResults:: [(Move, Game)] -> [(Move, Result)]
           allResults [] = []
           allResults ((m,g):gs) = (m, whoWillWin g):(allResults gs)
           allGamesM:: Game -> [(Move, Game)]
           allGamesM game = catMaybeTuples $ map (\m -> (m, ( moveGame game m))) (findValidMoves game)

           bestFor:: Player -> [(Move, Result)] -> (Move, Result)
           bestFor p res  = if (checkWin /= []) 
                            then head checkWin 
                            else if (checkTie /= []) 
                                 then head checkTie
                                 else head res
               where checkWin = filter (\(m, r) -> (Won p)==r ) res
                     checkTie = filter (\(m, r) -> r==Tie ) res

bestMove:: Game -> Move 
bestMove game = fst $ findBestMove game
    
moveGame:: Game -> Move -> Maybe Game
moveGame (Game b int player) m = 
     case (move b m) of 
         Nothing -> Nothing 
         Just board ->  Just (Game board (int-1) (opponent player))

--Check who will win
whoWillWin:: Game -> Result
whoWillWin game@(Game board int player) = 
  case gameResult game of
         Just result -> result
         Nothing -> mini player (allResults (allGames game))
     where allResults:: [Game] -> [Result]
           allResults games = map whoWillWin games 
           allGames:: Game -> [Game]
           allGames game = catMaybes $ map (moveGame game) (findValidMoves game)
           mini:: Player -> [Result] -> Result
           mini p res  = if ((Won p) `elem` res) 
                                then (Won p)
                                else if (Tie `elem` res) 
                                     then Tie else (Won (opponent p))
catMaybeTuples :: [(a, Maybe b)] -> [(a,b)]
catMaybeTuples ls = [(x,y) | (x, Just y) <- ls]

makeAiMove:: Game -> Game
makeAiMove game = fromJust $ moveGame game (bestMove game) 

--Cut-off depth minimax
cutOffDepth:: Int -> Game -> (Int, Move)
cutOffDepth depth game@(Game board int player) = bestFor player (allResults (allGamesM))
    where rating = evaluateGame game
          moves = findValidMoves game
          allGamesM:: [(Move, Game)]
          allGamesM = catMaybeTuples $ map (\m -> (m, moveGame game m)) moves 
          allResults:: [(Move, Game)] -> [(Int, Move)]
          allResults [] = []
          allResults ((m,g):gs) = (whoMightWin depth g, m):(allResults gs)
          bestFor White = maximum 
          bestFor Black = minimum


evaluateResult Tie = 0
evaluateResult (Won White) = 100000
evaluateResult (Won Black) = -100000

whoMightWin:: Int -> Game -> Int
whoMightWin 0 game = evaluateGame game
whoMightWin depth game@(Game board int player) =
         case gameResult game of
                Just res -> evaluateResult res
                Nothing -> bestFor player allScores
    where allScores:: [Int]
          allScores = map (whoMightWin (depth-1)) allGames 
          allGames:: [Game]
          allGames = catMaybes $ map (moveGame game) (findValidMoves game)
          bestFor:: Player -> [Int] -> Int 
          bestFor p res  =  
              case p of 
                     White -> maximum res 
                     Black -> minimum res

playAiMove:: Game -> Int -> Game
playAiMove game num = fromJust $ moveGame game (snd (cutOffDepth num game))
